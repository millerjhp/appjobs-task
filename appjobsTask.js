/* a) Create a function that returns the minimum and maximum numbers from an array of numbers , in that order. */

function minMax(givenArray) {
  let min = givenArray[0];
  let max = 0;

  givenArray.forEach(n => {
    if (n < min) { min = n; }
    if (n > max) { max = n; }
  });

  return [min, max];
}

minMax([1, 2, 3, 4, 5]); // [1, 5]
minMax([3445565, 2]); // [2, 3445565]
minMax([1]); // [1, 1]

/* b) Create a function that checks if the given string matches the requirements. */

function isValid(givenString) {
  if ((typeof Number(givenString) == 'number') && !(givenString.indexOf(' ') > 0) && (givenString.length <= 5)) {
    return true;
  } else {
    return false;
  }
}

isValid("69001"); // true
isValid("321a7"); // false
isValid("821 45"); // false
isValid("898898"); // false

/* c) Create a function that converts an object into an array, where each element represents a key-value pair. */

function toArray(givenObject) {
  return Object.entries(givenObject);
}

toArray({ a: 1, b: 2 }); // [["a", 1], ["b", 2]]
toArray({ dogs: 5, cats: 2 }); // [["dogs", 5], ["cats", 2]]
toArray({}); // []

/* d) Create a function that returns an array of objects from all "prerequisites" keys. */
function returnPrerequisits(givenArray) {
  let prerequisites = [];

  givenArray.forEach(key => {
    prerequisites.push(key.prerequisites);
  });

  return prerequisites;
}

returnPrerequisits([{ "id": 1, "name": "accommodation", "prerequisites": [{ "id": 1, "name": "Apartment/Flat" }, { "id": 2, "name": "House" }] }, { "id": 2, "name": "vehicle", "prerequisites": [{ "id": 3, "name": "Boat" }, { "id": 4, "name": "Truck" }] }]);
